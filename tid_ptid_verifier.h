/*
 * Use this module to put an assert at the location in your program where you wanna ensure that tid represents a pthread id it should.
 * Make sure to call init in this function at the last possible location just before you start executing ds ops.
 */

/* 
 * File:   tid_ptid_verifier.h
 * Author: a529sing
 *
 * Created on January 31, 2020, 12:57 PM
 */

#ifndef TID_PTID_VERIFIER_H
#define TID_PTID_VERIFIER_H

const uint max_tid = MAX_TID_POW2;//255;
static volatile pthread_t tid_ptid_map[max_tid];

/*
 * USAGE NOTICE: initialize last possible location just before DS operation would be executed.
 */
void store_ptid_at_tid(const int  tid)
{
    assert ("TID_PTID_VERIFIER_H:: tid >= max_tid"&&tid < max_tid);
    tid_ptid_map[tid] = pthread_self();
}

/*
 * USAGE NOTICE: call from the location where you wanna check if current tid is a valid pthread tid which you intended
 *                              to store for your recovery manager.
 */
pthread_t load_ptid_from_tid(const int tid)
{
    assert ("TID_PTID_VERIFIER_H:: tid >= max_tid"&&tid < max_tid);
    return tid_ptid_map[tid];
}
#endif /* TID_PTID_VERIFIER_H */

