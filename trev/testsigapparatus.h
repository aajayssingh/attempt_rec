/*
 * TODO:
 * Code still segfaults sometimes even though using sig_atomic_t (Though it is not stack smashing.... probably occurs due to access of a var after sigjmp or 
 * asynchrony between sig handler path and normal path)and removed all vul cout or sys calls. Only new call occurs where its guaranteed that long jmp wont occur due to
 * sig_atomic_t saftey check.
 * If I remove new memory aloc sys call the crash disappears.
 * 
 * Issue analysis with sys call new delete:
 * ASAN Reveqals following reasons:   pc points to zero page, stack overflow,  signal caused by read memory address. OR more informative is 
 * 871==ERROR: AddressSanitizer: alloc-dealloc-mismatch (INVALID vs operator delete []) on 0x604000000490  (hints at user level problem with my new delete alloc)
 * 
 * Issue analysis : with COUT only as sys call.
 * If I only call COUT then ASAN SIGSEGVs at setjmp write. says signal caused by a write.
 * Yet to be tested for multiple threads taking turns to signals. Right now only one threads sends signals that too enough distance apart in time.

 * NO ISSUE  if no system call executed. only for single thread sending kill slowly to others.
 * 
 * TRY: https://notes.shichao.io/apue/ch10/#sigsetjmp-and-siglongjmp-functions instead of manually setting setjmpbuffers_state at different places.
 * 
 * 
 *  */

/* 
 * File:   testsigapparatus.h
 * Author: a529sing
 *
 * Created on January 7, 2020, 11:13 AM
 */

#ifndef TESTSIGAPPARATUS_H
#define TESTSIGAPPARATUS_H
#include <cassert>
#include <csignal>
#include "locks_impl.h"
#include "lazylist.h"
#include "testsigapparatus.h"
#include <cerrno>
/*
 * http://man7.org/linux/man-pages/man7/signal-safety.7.html
        1. Ensure that (a) the signal handler calls only async-signal-safe
          functions, and (b) the signal handler itself is reentrant with
          respect to global variables in the main program.

       2. Block signal delivery in the main program when calling functions
          that are unsafe or operating on global data that is also accessed
          by the signal handler.
   Following happened when I used cout or new for the sig receiver threads.    
 *   If a signal handler interrupts the execution of an unsafe
          function, and the handler terminates via a call to longjmp(3) or
          siglongjmp(3) and the program subsequently calls an unsafe
          function, then the behavior of the program is undefined.
 */
template <typename K, typename V, class RecManager>
bool lazylist<K,V,RecManager>::dummyOp(const int tid, const K& key) 
{
    volatile int i0 = 0;
    volatile int i0_send_sig_once_every=10000;
    while(true)
    {
        //FIX ME: If without saving the env siglongjmp is called I will get ndef crashes as env restored is bogus. (sj3) http://web.eecs.utk.edu/~jplank/plank/classes/cs360/360/notes/Setjmp/lecture.html

    //    if(sigsetjmp(setjmpbuffers[tid], 1/*to save sig mask too along with env(pc, fp registers)*//*0*/) )  
        /*https://stackoverflow.com/questions/10330419/using-siglongjmp-effectively  sig mask 2nd param if 
         * 1 then calliing threads sigmask aslo saved that is useful when longjmp from handler is made. The signal automatically gets unblocked.*/
        
        /*volatile int saveenv */ setjmpbuffers_state[tid]= sigsetjmp(setjmpbuffers[tid], 1/*to save sig mask too along with env(pc, fp registers)*//*0*/);
        /*Cannot have setjmpbuffers_state as lhs of setjmp() invalid env may occur due to longjmo in handler*/
        //****setjmpbuffers_state[tid] = 0; //means longjmp can occur now from handler(Vulnerable: What if signal received in alternate path long jmp can occur as bufstate is 0 but setjmp hasnt \
                                                   ocuurred for that.). BUG BUG: more than 1 consecutive longjmps  will occur with just one setjmp. Undefined behaviour. FIX ME.
        //    __sync_synchronize();
        if (/*saveenv*/ setjmpbuffers_state[tid] == 1)//if means long jmp has occurred.
        {
            if (tid != 2){ 
                unsigned int sl1 = 100;
                while(sl1--){;}
//                int* n = new int(1);
            }
             dummyinHz[tid] = 0;//false;
            
            
//             COUTATOMICTID("ALTERNATE PATH: BEGIN"<<endl);
    //         this_thread::sleep_for(chrono::seconds (1));
//            COUTATOMICTID("+++++++++++++++++++++++++++++++++++++++++ALTERNATE PATH: setjmpbuffers_state[tid]"<<setjmpbuffers_state[tid]<<endl);
    //        COUTATOMICTID("ALTERNATE PATH: END"<<setjmpbuffers_state[tid]<<endl);
//            continue; //setjmpbuffers_state will be 1 untill next setjmp call. This means siglong jump wont occur till then.
        }
        else
        {
            /*to simulate scene where all threads have high probability of having valid longjmp env 
             * (ie after setjmp and not at  before setjmp while receiving signal.).
             */
            if (tid != 2){ 
                unsigned int sl1 = 5000;
                while(sl1--){
                    if (sl1%500 == 0)
                    {
                        //Test to see that async sig safe sys calls cause SEGV or undefined behaviour in code.
//                    //COUTATOMICTID("Receiver"<<sl1<<endl);
//                    //if (errno == EINTR)
//                        new int(1);
//                        cout<<"OOOOOOOOOOOOOOOOOOO"<<errno<<endl;
                    //sig block
//                    sigset_t set;
//                    sigset_t oldset;
//                    sigemptyset(&set);
//                    sigemptyset(&oldset);
//                    sigaddset(&set, SIGQUIT); //does union with existing mask
                    dummyinHz[tid] = 1;
//                    if( __sync_bool_compare_and_swap(&dummyinHz[tid], false, true) ){
                    
                    /*VULNERABLE: What happens if a signal has been generated but not delivered to this thread. And this thread in middle changes the signal mask??*/
                     /* Some insight on why program crashes with siglongjmp enabled and changing sig mask
                      * http://poincare.matf.bg.ac.rs/~ivana/courses/ps/sistemi_knjige/pomocno/apue/APUE/0201433079/ch12lev1sec8.html
                      * Another thread may undo the blocking of sig by installing sig handler. --> But with two threads also I get crash Which implies their is no thread to restore default sig disposition
                      * thus not undoing the temp masking of sig
                      */
                        //NOT safe to call what if sig is generated and not yet or is about to be delivered. Changing sigmask would concur with assigning sig handler path.
//                     int rc = pthread_sigmask(SIG_BLOCK, &set, &oldset); //cannot have siglong jmp enabled while setting mask though can receive signals.
//                    if (rc) { //AJ DOes this unblock signal for all threads or only for the calling thread?
//                        COUTATOMICTID("ERROR BLOCKING SIGNAL"<<rc<<endl);
//                        exit(-1);
//                    }
//                    else
//                    {
////                        COUTATOMICTID("BLOCKING SIGNAL"<<rc<<endl);
//                    }
//                    
////                    can not receive signals while doing sys calls - COUT, new delete.
////                    int *n = new int[1000];
////                    delete[] n;
//                    
//                    if (pthread_sigmask(SIG_SETMASK, &oldset, NULL)) { //AJ DOes this unblock signal for all threads or only for the calling thread?
//                        COUTATOMICTID("ERROR UNBLOCKING SIGNAL"<<endl);
//                        exit(-1);
//                    }
//                    else
//                    {
////                        COUTATOMICTID("UNBLOCKING SIGNAL"<<endl);
//                    }
                    
//                    int *n = new int[10];
//                    delete[] n;
                    
//                        COUTATOMICTID("dummyinHz[tid]"<<dummyinHz[tid]<<endl);
//                        dummyinHz[tid] = false;
//                    __sync_bool_compare_and_swap(&dummyinHz[tid], true, false);
//                    }
                    dummyinHz[tid] = 0;    
                   //sig unblock
                    }
                }
            }

            if ( ( 2 == tid && (--i0_send_sig_once_every == 0) ) ) //test behaviour if only one thread sends signal
            {//BEGIN: NeutralizeALL one thread
    //           this_thread::sleep_for(chrono::seconds (5));//wait to execute normal path again
    //        unsigned int sl1 = 1000000;
    //        while(sl1--);

                int otherTid = 0;
                for(otherTid = 0; otherTid < recordmgr->recoveryMgr->NUM_PROCESSES; ++otherTid)//TODO: change thread id to uint type. VPT:********Assumed tid BEGINS WITH 0.********
                {
                    //send signal to other tids
                    if(tid != otherTid )//dont send sig to self
                    {
                        pthread_t otherPthread = recordmgr->recoveryMgr->getPthread(otherTid);
                        int error = 0;
//                        COUTATOMICTID("GONNA pthread_kill. i0_send_sig_once_every:"<<i0_send_sig_once_every<<endl);
                        if (error = pthread_kill(otherPthread, recordmgr->recoveryMgr->neutralizeSignal) )//SIGQUIT
                        {
                            COUTATOMICTID("ERROR "<<error<<" when trying to pthread_kill(pthread_tFor("<<otherTid<<"), "<<recordmgr->recoveryMgr->neutralizeSignal<<")"<<std::endl);
    //                        assert("signal not sent"&&0);
                        }
                        else
                        {
                            COUTATOMICTID("Signal sent via pthread_kill(pthread_tFor("<<otherTid<<"), "<<recordmgr->recoveryMgr->neutralizeSignal<<")"<<++i0<<std::endl);

                        }

                        if (EINVAL == error)
                        {
                            COUTATOMICTID("EINVAL invalid signal id to pthread_kill."<<endl);
                        }
                        else if (ESRCH == error)
                        {
                            COUTATOMICTID("ESRCH a thread Id accessed beyond its lifetme."<<endl);
                        }
                        else if (EINTR == error)
                        {
                            COUTATOMICTID("EINTR unthinkable should not happen:interrupted during this system call. RESTART. \
                                Or Autorestart by specifying flag in sigaction."<<endl);
                        }                    
                    }//if(tid != otherTid)        
                }//for(int otherTid = 0; otherTid < this->
                 i0_send_sig_once_every = 100000; //reset. Allows T1 to finish. So siglongjmp doesnt occur consecutively.
//            COUTATOMICTID("Wait for next turn to neutralize all"<<endl<<endl);
//            this_thread::sleep_for(chrono::seconds (1));//wait to execute normal path again
            }//END: NeutralizeALL one thread

//            if (4 == tid)
//                if(i0_send_sig_once_every%10000 == 0)
//                COUTATOMICTID("i0_send_sig_once_every:"<<i0_send_sig_once_every<<endl);
        }

        /*signal cannot be received (semantically siglongjmp will not occur.). Make sure that volatile works 
         * (occurs atomically and in handler valid value is seen) may be use CAS.*/
        setjmpbuffers_state[tid] = -1; 
    }//while
}

#endif /* TESTSIGAPPARATUS_H */

