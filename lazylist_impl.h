/**
 *   This algorithm is based on: 
 *   A Lazy Concurrent List-Based Set Algorithm,
 *   S. Heller, M. Herlihy, V. Luchangco, M. Moir, W.N. Scherer III, N. Shavit
 *   OPODIS 2005
 *   
 *   The implementation is based on implementations by:
 *   Vincent Gramoli https://sites.google.com/site/synchrobench/
 *   Vasileios Trigonakis http://lpd.epfl.ch/site/ascylib - http://lpd.epfl.ch/site/optik
 */

#ifndef LAZYLIST_IMPL_H
#define	LAZYLIST_IMPL_H

#include <cassert>
#include <csignal>
#include "locks_impl.h"
#include "lazylist.h"
#include "testsigapparatus.h"

#ifndef casword_t
#define casword_t uintptr_t
#endif


template <typename K, typename V, class RecManager>
lazylist<K,V,RecManager>::lazylist(const int numProcesses, const K _KEY_MIN, const K _KEY_MAX, const V _NO_VALUE)
        : recordmgr(new RecManager(numProcesses, /*SIGRTMIN+1*/SIGQUIT)), KEY_MIN(_KEY_MIN), KEY_MAX(_KEY_MAX), NO_VALUE(_NO_VALUE)
#ifdef USE_DEBUGCOUNTERS
        , counters(new debugCounters(numProcesses))
#endif
{
    const int tid = 0;
    //initThread(tid); // this call was making tree->initthread() for tid 0 a NOP. Note that initThread can only be called only once due to a check inits first line. Subsequent calls will be a NOP.
    nodeptr max = new_node(tid, KEY_MAX, 0, NULL);
    head = new_node(tid, KEY_MIN, 0, max);
    COUTATOMIC("lazylist: CTOR"<<endl);
}

template <typename K, typename V, class RecManager>
lazylist<K,V,RecManager>::~lazylist() {
    const int dummyTid = 0;
    nodeptr curr = head;
    while (curr->key < KEY_MAX) {
        nodeptr next = curr->next;
        recordmgr->deallocate(dummyTid, curr);
        curr = next;
    }
    recordmgr->deallocate(dummyTid, curr);
    
    recordmgr->printStatus();
    
    delete recordmgr;
    #ifdef USE_DEBUGCOUNTERS
    delete counters;
#endif
}

template <typename K, typename V, class RecManager>
void lazylist<K,V,RecManager>::initThread(const int tid) {
    if (init[tid]) return; else init[tid] = !init[tid];

    recordmgr->initThread(tid);
}

template <typename K, typename V, class RecManager>
void lazylist<K,V,RecManager>::deinitThread(const int tid) {
    if (!init[tid]) return; else init[tid] = !init[tid];

    recordmgr->deinitThread(tid);
}

template <typename K, typename V, class RecManager>
nodeptr lazylist<K,V,RecManager>::new_node(const int tid, const K& key, const V& val, nodeptr next) {
    nodeptr nnode = recordmgr->template allocate<node_t<K,V> >(tid);
    if (nnode == NULL) {
        cout<<"out of memory"<<endl;
        exit(1);
    }
    nnode->key = key;
    nnode->val = val;
    nnode->next = next;
    nnode->lock = false;
    nnode->marked = 0;

    return nnode;
}

template <typename K, typename V, class RecManager>
inline int lazylist<K,V,RecManager>::validateLinks(const int tid, nodeptr pred, nodeptr curr) {
    //COUTATOMICTID(" validateLinks::"<<(!pred->marked && !curr->marked && pred->next == curr)<<endl);
//    if (! (!pred->marked && !curr->marked && pred->next == curr) )
//        COUTATOMICTID("---------------------------------------------------------------->validation failed");
    return (!pred->marked && !curr->marked && pred->next == curr);
}

template <typename K, typename V, class RecManager>
bool lazylist<K,V,RecManager>::contains(const int tid, const K& key) {
/*
 * Need a goto here when a tid receives signal inside this it needs to restart from head. Use GOTO Label.
 */
#if SIMPLE_RECOVERY_APPARATUS
//testing recovery manager
    return dummyOp(tid, key);
#else
    
    retry_contains:
    setjmpbuffers_state[tid] = sigsetjmp(setjmpbuffers[tid], 1/*to save sig mask too along with env(pc, fp registers)*//*0*/);
    if (/*saveenv */ setjmpbuffers_state[tid] == 1)//if 1 means long jmp has occurred.
    {
//        COUTATOMICTID("!!!!!!!!ALTERNATE PATH!!!!!!! ::contains:: key:"<<key<<endl);
        recordmgr->enterQuiescentState(tid); //1. clears any acquired hz ptrs. 2. sets in Hz sect false (this is not needed but kind of unnecessary action done due to call to funct)
//        COUTATOMICTID("!!!!!!!!ALTERNATE PATH-EQ!!!!!!! ::contains:: key:"<<key<<endl);
        goto retry_contains;
    }
    else
    {
        recordmgr->leaveQuiescentState(tid);
        nodeptr curr = head;
        while (curr->key < key) {
            curr = curr->next;
        }

        V res = NO_VALUE; 
        if ((curr->key == key) && !curr->marked) {
            res = curr->val;
        }
        recordmgr->enterQuiescentState(tid);
        return (res != NO_VALUE);
    }
    assert ("Contains() shouldn't reach here."&&0);

#endif// #ifdef SIMPLE_RECOVERY_APPARATUS
}

template <typename K, typename V, class RecManager>
V lazylist<K,V,RecManager>::doInsert(const int tid, const K& key, const V& val, bool onlyIfAbsent) {
    nodeptr curr;
    nodeptr pred;
    nodeptr newnode;
    V result;
    
    while (true) {
//        COUTATOMICTID("doInsert:: key:"<<key<<endl);
//        unsigned long i = 100000;
//        while(--i){
//            //COUTATOMICTID(i<<endl);
//        }
//        if (0 == tid)
//            this_thread::sleep_for(chrono::seconds (1));
        
        //############
        /*volatile int saveenv */ setjmpbuffers_state[tid] = sigsetjmp(setjmpbuffers[tid], 1/*to save sig mask too along with env(pc, fp registers)*//*0*/);
         // **** setjmpbuffers_state[tid] = 0; //means longjmp can occur now from handler(Vulnerable: What if signal received in alternate path long jmp can occur as bufstate is 0 but setjmp hasnt \
                                               ocuurred for that.). BUG BUG: more than 1 consecutive longjmps  will occur with just one setjmp. Undefined behaviour. FIX ME: use jmp as lhs of setjmp.
        if (/*saveenv */ setjmpbuffers_state[tid] == 1)//if 1 means long jmp has occurred.
        {
            /*
             * Release any acquired HzPtr
             * Not needed to set setjmpbuffers_state[tid] = -1. Since here its already 1. Thus cannot be longjumped untill reset to 0 by subsequent call to setjmp. Thus 
             * setjmp constraints remain valid.
             * setjmpbuffers_state[tid] remains 1 all time untill reset by setjmp. Thus cannot long without a presecing setjmp as longjmp occurs only if setjmpbuffers_state[tid] ==0
             */

            //COUTATOMICTID("!!!!!!!!ALTERNATE PATH!!!!!!! ::doInsert:: key:"<<key<<endl);
            recordmgr->enterQuiescentState(tid); //already called in handler
                                                                        //1. clears any acquired hz ptrs. 2. sets in Hz sect false (this is not needed but kind of unnecessary action done due to call to funct)
//             COUTATOMICTID("!!!!!!!!ALTERNATE PATH-EQ!!!!!!! ::doInsert:: key:"<<key<<endl);
             continue;
        }//############
        else
        {
            recordmgr->leaveQuiescentState(tid);  //checks if retiredbag full, neutralize all free ret bag.
            pred = head;
            curr = pred->next;
            while (curr->key < key) {
                pred = curr;
                curr = curr->next;
            }
            
            //REMOVEME
//            this_thread::sleep_for(chrono::seconds (1));

            /*
             * Design decision: 
             * 1. To acquire Hzp first and then annonce Hz write section? or
             * 2. To announce Hz write section first and then acquire Hp?
             * If taken path 1: while restarting (since Hz writesection hasnt been reached yet and we dont restart only in Hzwrite sect)
             *                        I may have to release Hz pointers .. do some cleanup to restart the op.
             * If taken path 2: When update received a signal while acquiring Hzp it will be allowed to resume after signal though it doesnt have Hzp yet. Thus before 
             * `                     Hzp is taken reclaimer may free it. This is incorrect. So only path 1 choice is valid.
             */

            //protect the nodes or any record accesed within Hz write section. this avoid reclaimer freeing them after sending signals and thread resuming 
            //hzwrite section may access a reclaimed memory without being hampered.
            recordmgr->qProtect(tid, pred, (CallbackType) callbackReturnTrue, NULL);

            //beginHzwrite: the pointer after which you dont want opn to restart due to signal/
            recordmgr->protect(tid, (nodeptr)NULL, NULL, NULL);
            acquireLock(&(pred->lock));
            if (validateLinks(tid, pred, curr)) {
                if (curr->key == key) { //key is in list
                    // node containing key is not marked
                    V result = NO_VALUE; //failed -1
                    releaseLock(&(pred->lock));
                    recordmgr->enterQuiescentState(tid); //also ends Hzwritesect and releases all Hzp in qUnprotectAll()
                    return result; //failed
                }
                // key is not in list
                assert(curr->key != key);
                result = curr->val;
                newnode = new_node(tid, key, val, curr);
                pred->next = newnode;

                releaseLock(&(pred->lock));
                recordmgr->enterQuiescentState(tid); //end rec. //also ends Hzwritesect and releases all Hzp in qUnprotectAll()
                return result;
            }
            else
            {
                /*        
                if(pred->marked)
                {
                    TRACE COUTATOMICTID("insert:: validation failed:pred->marked"<<pred->marked<<endl);
                }
                else if(curr->marked){
                    TRACE COUTATOMICTID("insert:: validation failed:curr->marked"<<curr->marked<<endl);
                }
                else if(pred->next != curr){
                    TRACE COUTATOMICTID("insert:: validation failed:pred->next != curr"<<endl);
                }
                */

            }

            releaseLock(&(pred->lock));
            recordmgr->enterQuiescentState(tid); //end rec. //also ends Hzwritesect and releases all Hzp in qUnprotectAll()
        }
        COUTATOMICTID("RETRYING... key: "<<key<<endl);
    } //while (true)
}

/*
 * Logically remove an element by setting a mark bit to 1 
 * before removing it physically.
 */
template <typename K, typename V, class RecManager>
V lazylist<K,V,RecManager>::erase(const int tid, const K& key) {
    nodeptr pred;
    nodeptr curr;
    V result;
    while (true) {
//        COUTATOMICTID("erase:: key:"<<key<<endl);
//        unsigned long i = 100000;
//        while(--i){
//            //COUTATOMICTID(i<<endl);
//        }
        
                //############ if(recordmgr->supportsCrashRecovery());
        setjmpbuffers_state[tid] = sigsetjmp(setjmpbuffers[tid], 1/*to save sig mask too along with env(pc, fp registers)*//*0*/);
        if(/*saveenv */ setjmpbuffers_state[tid] == 1 ){
            //COUTATOMICTID("!!!!!!!!ALTERNATE PATH!!!!!!! ::erase:: key:"<<key<<endl);
            recordmgr->enterQuiescentState(tid); //already called in handler
                                                                        //1. clears any acquired hz ptrs. 2. sets in Hz sect false (this is not needed but kind of unnecessary action done due to call to funct)
//            COUTATOMICTID("!!!!!!!!ALTERNATE PATH-EQ!!!!!!! ::erase:: key:"<<key<<endl);
            continue;
        }//############
        else
        {
            recordmgr->leaveQuiescentState(tid);
            pred = head;
            curr = pred->next;
            while (curr->key < key) {
                pred = curr;
                curr = curr->next;
            }

            //protect the nodes or any record accesed within Hz write section. As reclaimer may reclaim them after sending signals and thread resuming 
            //hzwrite section may access a reclaimed memory.
            recordmgr->qProtect(tid, pred, (CallbackType) callbackReturnTrue, NULL);
            recordmgr->qProtect(tid, curr, (CallbackType) callbackReturnTrue, NULL);

            //beginHzwrite: the pointer after which you dont want opn to restart due to signal/
            recordmgr->protect(tid, (nodeptr)NULL, NULL, NULL);
            if (curr->key != key) {
                result = NO_VALUE;
                recordmgr->enterQuiescentState(tid); //qunprotect() hijacked this API to release all Hzp. like end Hzwrite sect()
                return result;
            }

            acquireLock(&(pred->lock));
            acquireLock(&(curr->lock));
            if (validateLinks(tid, pred, curr)) {
                assert(curr->key == key);
                result = curr->val;
                nodeptr c_nxt = curr->next;

                curr->marked = 1;                                                   // LINEARIZATION POINT
                pred->next = c_nxt;

                recordmgr->retire(tid, curr);

                releaseLock(&(curr->lock));
                releaseLock(&(pred->lock));
                recordmgr->enterQuiescentState(tid); //qunprotect() hijacked this API to release all Hzp. like end Hzwrite sect()
                return result;
            }
            else
            {
                /*
                if(pred->marked)
                {
                    TRACE COUTATOMICTID("erase:: validation failed:pred->marked"<<pred->marked<<endl);
                }
                else if(curr->marked){
                    TRACE COUTATOMICTID("erase:: validation failed:curr->marked"<<curr->marked<<endl);
                }
                else if(pred->next != curr){
                    TRACE COUTATOMICTID("erase:: validation failed:pred->next != curr"<<endl);
                }
                */

            }

            releaseLock(&(curr->lock));
            releaseLock(&(pred->lock));
            recordmgr->enterQuiescentState(tid);//qunprotect() hijacked this API to release all Hzp. like end Hzwrite sect()
        }//else
        COUTATOMICTID("RETRYING..."<<endl);
    }//While
}

template <typename K, typename V, class RecManager>
long long lazylist<K,V,RecManager>::debugKeySum(nodeptr head) {
    long long result = 0;
    nodeptr curr = head->next;
    while (curr->key < KEY_MAX) {
        result += curr->key;
        curr = curr->next;
    }
    return result;
}

template <typename K, typename V, class RecManager>
long long lazylist<K,V,RecManager>::debugKeySum() {
    return debugKeySum(head);
}

#endif	/* LAZYLIST_IMPL_H */

