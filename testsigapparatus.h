/*
 * TODO:
 * Code still segfaults sometimes even though using sig_atomic_t (Though it is not stack smashing.... probably occurs due to access of a var after sigjmp or 
 * asynchrony between sig handler path and normal path)and removed all vul cout or sys calls. Only new call occurs where its guaranteed that long jmp wont occur due to
 * sig_atomic_t saftey check.
 * If I remove new memory aloc sys call the crash disappears.
 * 
 * Issue analysis with sys call new delete:
 * ASAN Reveqals following reasons:   pc points to zero page, stack overflow,  signal caused by read memory address. OR more informative is 
 * 871==ERROR: AddressSanitizer: alloc-dealloc-mismatch (INVALID vs operator delete []) on 0x604000000490  (hints at user level problem with my new delete alloc)
 * 
 * Issue analysis : with COUT only as sys call.
 * If I only call COUT then ASAN SIGSEGVs at setjmp write. says signal caused by a write.
 * Yet to be tested for multiple threads taking turns to signals. Right now only one threads sends signals that too enough distance apart in time.

 * NO ISSUE  if no system call executed. only for single thread sending kill slowly to others.
 * 
 * TRY: https://notes.shichao.io/apue/ch10/#sigsetjmp-and-siglongjmp-functions instead of manually setting setjmpbuffers_state at different places.
 * 
 * 
 *  */

/* 
 * File:   testsigapparatus.h
 * Author: a529sing
 *
 * Created on January 7, 2020, 11:13 AM
 */

/*
 *FOLLOW ME: No printf or non signal safe calls in Non Haz section bcse a longjmp would change control flow and sys call could nt be restarted thus hang may occur.
 */


#pragma once

#include <cassert>
#include <csignal>
#include <cerrno>
#include <atomic>
#include "locks_impl.h"
#include "lazylist.h"
#include "tid_ptid_verifier.h"
/*6
 * http://man7.org/linux/man-pages/man7/signal-safety.7.html
        1. Ensure that (a) the signal handler calls only async-signal-safe
          functions, and (b) the signal handler itself is reentrant with
          respect to global variables in the main program.

       2. Block signal delivery in the main program when calling functions
          that are unsafe or operating on global data that is also accessed
          by the signal handler.
   Following happened when I used cout or new for the sig receiver threads.    
 *   If a signal handler interrupts the execution of an unsafe
          function, and the handler terminates via a call to longjmp(3) or
          siglongjmp(3) and the program subsequently calls an unsafe
          function, then the behavior of the program is undefined.
 */

#define MYFENCE __sync_synchronize(); //asm volatile("mfence":::"memory");//std::atomic_thread_fence();
#define MYBOOL_CAS __sync_bool_compare_and_swap
#define NUM_THR  143//MAX_TID_POW2 //128//3//7//6

//Begin DEBUG Vars
static volatile int sigh_ljmpcount[NUM_THR]={0};//{0,0,0};//{0,0,0,0,0,0,0};//{0,0,0,0}; //siglongjmp count
static volatile int sigh_execcount[NUM_THR]={0};//{0,0,0};//{0,0,0,0,0,0,0};//{0,0,0,0};//caught in handler count
static volatile long int sender_count[NUM_THR]={0};//{0,0,0,0,0,0,0};//{0,0,0,0}; //sender count
static volatile long int restart_count[NUM_THR]={0};//{0,0,0,0,0,0,0};//{0,0,0,0};//alternatepath count
static volatile long int hzblock_count[NUM_THR]={0};//{0,0,0,0,0,0,0};//{0,0,0,0}; //sender count
volatile int sparetid = NUM_THR - 1;
//End DEBUG Vars

static volatile sig_atomic_t setjmp_res[NUM_THR] ={1};//{1,1,1};//{1,1,1,1,1,1,1};//{1,1,1,1};
static volatile sig_atomic_t /*int*/  jmp_allowed[NUM_THR] ={1};//{1,1,1};//{1,1,1,1,1,1,1};//{1,1,1,1};

/*FOLLOWME: Should be called before barrier releases all threads to execute op inside benchmark*/
void initAllStatVars(const int tid){
    hzblock_count[tid] = 0;
    restart_count[tid] = 0;
    sender_count[tid] = 0;
    
    sigh_ljmpcount[tid]=0;
    sigh_execcount[tid]=0;
    setjmp_res[tid] = 1;
    jmp_allowed[tid] = 1;
}

template <class MasterRecordMgr>
extern void dummycrashhandler(int signum, siginfo_t *info, void *uctx){
    MasterRecordMgr * const recordmgr = (MasterRecordMgr * const) ___singleton;
    int tid = (int) ((long) pthread_getspecific(pthreadkey));
    #if TID_PTID_VERIFIER
        assert("pthread_self() not eq registeredThreads[tid]" && pthread_self() == registeredThreads[tid]  );
        assert("pthread and tid map check" && pthread_self() == load_ptid_from_tid(tid)  );
        //assert("pthread and tid map check" && (pthread_self() == load_ptid_from_tid(tid)) && (registeredThreads[tid] == pthread_self()) );
    #endif
    sigh_execcount[tid]++;

    int mytemp =  jmp_allowed[tid];
    if (setjmp_res [tid] == 0 && /*mytemp == 1 &&*/ jmp_allowed[tid] == 1){
        sigh_ljmpcount[tid]++;

        assert("jmp_allowed" &&jmp_allowed[tid] == 1);
        assert("setjmp_res [tid] == 0"&& setjmp_res [tid] == 0);
//        printf("tid=%d mytemp=%d jmp_allowed %d sigh_execcount[0]=%d sigh_execcount[1]=%d sigh_ljmpcount[0]=%d sigh_ljmpcount[1]=%d pthread%ul \n", tid, mytemp, jmp_allowed[tid], sigh_execcount[0], sigh_execcount[1], ii[0], ii[1], pthread_self());
        siglongjmp(setjmpbuffers[tid], 1);
    }
}

template <typename K, typename V, class RecManager>
bool lazylist<K,V,RecManager>::dummyOp(const int tid, const K& key) 
{
    assert("check to also change NUM_THR with cmd -n" && recordmgr->NUM_PROCESSES == NUM_THR);
    static thread_local int k = 0;
    static thread_local int spareth_excount = 0;

    assert("ensure run with NUM_THR"&& setjmp_res[tid] == 1 and jmp_allowed[tid] == 1);

    while(true) 
    {
        #if TID_PTID_VERIFIER
        assert("pthread and tid map check" && (pthread_self() == load_ptid_from_tid(tid)) && (registeredThreads[tid] == pthread_self()) );
        #endif
        
        if (setjmp_res[tid] = sigsetjmp(setjmpbuffers[tid], 1))
        {   //RESTART BLOCK
            restart_count[tid]++;            
        }
        else
        {   //NORMAL BLOCK
            assert ("check tid not more than NUM_THR" && tid < NUM_THR);
            if(sparetid != tid)
            {
                
                //int other = 1-tid;
                assert ("check tid not more than NUM_THR-1 (last is spare tid)" && tid < NUM_THR-1);
                assert("verify spare tid is not executing here " && tid != sparetid );
                for(int other = 0; other < NUM_THR -1; other++)
                {
                    assert ("verify othertid is not equal to sparetid " && other != sparetid);
                    if (tid != other ) 
                    {     // sender
                        pthread_t otherPthread = recordmgr->recoveryMgr->getPthread(other);
                        ++k;

                        if ((k % 10000) == 0)
                        {
                            sender_count[tid]++;
                            
                            if (pthread_kill(otherPthread, SIGQUIT)) cout<<"error in pthread_kill"<<"tid="<<tid<<"\n";
                            //NOTICME:  
                            //from sender side only async-signal-safe functions could be called and printf is not async-signal-safe function, thus a siglongjump in mid of printf would cause hang..
                            //printf ("k=%d sender_count[0]=%d sender_count[1]=%d   sigh_ljmpcount[0]=%d sigh_ljmpcount[1]=%d   sigh_execcount[0]=%d sigh_execcount[1]=%d  hzblock_count[0]=%d hzblock_count[1]=%d   tid=%d pthreadself=%ul\n", k, sender_count[0], sender_count[1], ii[0], ii[1], cc[0], cc[1], rr[0], rr[1], tid, pthread_self());
                        }                
                    } 
                    else 
                    {    // receiver
                        assert(MYBOOL_CAS (&jmp_allowed[tid], 1, 0));//  = 0;
                        {   //HZ BLOCK: non restartable
                            volatile int * volatile n = new int[10];       
                            hzblock_count[tid]++;
                            delete[] n;
                        }
                        assert(MYBOOL_CAS (&jmp_allowed[tid], 0, 1));
                    }//receiver
               }//for kill each
            
            } //if(sparetid != tid)
            
            else{
                //DEBUG BLOCK: spare thread prints debug info
                assert("check to also change NUM_THR with cmd -n" && recordmgr->NUM_PROCESSES == NUM_THR);
                assert ("check sparetid is last tid" && sparetid == NUM_THR - 1);
                ++spareth_excount;
                if (spareth_excount%10000 == 0){
                    for (int i = 0; i < NUM_THR; i++)
                        printf ("#killed[%d]=%ld #died[%d]=%ld | ", i, sender_count[i], i, restart_count[i]);
                cout<<endl;
                }
            }
        } //normal block end
    }//while(true)
}
