/* 
 * File:   lazylist.h
 * Author: trbot
 *
 * Created on February 17, 2016, 7:34 PM
 */

#ifndef LAZYLIST_H
#define	LAZYLIST_H
#include "recordmgr/record_manager.h"
#include "recordmgr/recovery_manager.h"

template<typename K, typename V>
class node_t {
public:
    K key;
    volatile V val;
    node_t<K,V> * volatile next;
    volatile int lock;
    volatile long long marked; // is stored as a long long simply so it is large enough to be used with the lock-free RQProvider (which requires all fields that are modified at linearization points of operations to be at least as large as a machine word)
};

#define nodeptr node_t<K,V> *

template <typename K, typename V, class RecManager>
class lazylist {
private:
    RecManager * const recordmgr;
    #ifdef USE_DEBUGCOUNTERS
    debugCounters * const counters;
    #endif

    nodeptr head;

    int validateLinks(const int tid, nodeptr pred, nodeptr curr);
    nodeptr new_node(const int tid, const K& key, const V& val, nodeptr next);
    long long debugKeySum(nodeptr head);

    V doInsert(const int tid, const K& key, const V& value, bool onlyIfAbsent);
    
    int init[MAX_TID_POW2] = {0,};

public:
    const K KEY_MIN;
    const K KEY_MAX;
    const V NO_VALUE;
    lazylist(int numProcesses, const K _KEY_MIN, const K _KEY_MAX, const V NO_VALUE);
    ~lazylist();
    bool contains(const int tid, const K& key);
    
    bool dummyOp(const int tid, const K& key); 
    
    V insert(const int tid, const K& key, const V& value) {
        return doInsert(tid, key, value, true /*only if absent*/);
    }
//    V insertIfAbsent(const int tid, const K& key, const V& value) {
//        return doInsert(tid, key, value, true);
//    }
    V erase(const int tid, const K& key);
    
    /**
     * This function must be called once by each thread that will
     * invoke any functions on this class.
     * 
     * It must be okay that we do this with the main thread and later with another thread!!!
     */
    void initThread(const int tid);
    void deinitThread(const int tid);
#ifdef USE_DEBUGCOUNTERS
    debugCounters * debugGetCounters() { return counters; }
    void clearCounters() { counters->clear(); }
#endif
    
    long long debugKeySum();
    bool validate(const long long keysum, const bool checkkeysum) {
        return true;
    }

    long long getSizeInNodes() {
        long long size = 0;
        for (nodeptr curr = head->next; curr->key != KEY_MAX; curr = curr->next) {
            ++size;
        }
        return size;
    }
    long long getSize() {
        long long size = 0;
        for (nodeptr curr = head->next; curr->key != KEY_MAX; curr = curr->next) {
            size += (!curr->marked);
        }
        return size;
    }
    string getSizeString() {
        stringstream ss;
        ss<<getSizeInNodes()<<" nodes in data structure";
        return ss.str();
    }
    
    RecManager * const debugGetRecMgr() {
        return recordmgr;
    }
    
    node_t<K,V> * debug_getEntryPoint() { return head; }
};

#include "lazylist_impl.h"
#endif	/* LAZYLIST_H */

