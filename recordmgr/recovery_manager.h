/**
 * Implementation of a Record Manager with several memory reclamation schemes.
 * This file essentially implements the neutralizing / crash recovery mechanism
 * used by DEBRA+.
 *
 * The actual implementation of neutralizing uses sigsetjmp/siglongjmp.
 * Consequently, the crash recovery mechanism requires a "free" signal.
 * Technically, spuriously sending that signal will not break anything.
 * It will just slow a thread down by neutralizing it (if it is in a 
 * non-quiescent state).
 *
 * WARNING: this implementation of neutralizing only works for a SINGLE instance
 *       of record_manager, which must be globally available in a signal handler.
 *       There are simple ways to modify it to work with multiple record_manager
 *       instances. E.g., we can maintain a global list of all record_manager
 *       instances for access by the signal handler, and then have each thread
 *       executing the signal handler search this list to find the relevant
 *       instance (namely, the one where the thread is in a non-quiescent state).
 *       
 * Copyright (C) 2016 Trevor Brown
 * Contact (tabrown [at] cs [dot] toronto [dot edu]) with any questions or comments.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RECOVERY_MANAGER_H
#define RECOVERY_MANAGER_H

#include <cassert>
#include <csignal>
#include <setjmp.h>
#include "globals.h"
#include "debugcounter.h"

#define SIMPLE_RECOVERY_APPARATUS 1

// for crash recovery
static pthread_key_t pthreadkey;
static struct sigaction ___act;
static void *___singleton = NULL;
//extern pthread_key_t pthreadkey;
//extern struct sigaction ___act;
//extern void *___singleton;

static pthread_t registeredThreads[MAX_TID_POW2];
static void *errnoThreads[MAX_TID_POW2];
static sigjmp_buf *setjmpbuffers;

/*If used normal int in place of sig_atomic no guarantee that  read/write to these vars would be in one instruction (atomic)
 https://notes.shichao.io/apue/ch10/#sigsetjmp-and-siglongjmp-functions
 * Compulsory to use sig_atomic_t for safe siglongjmp and ensure matching of setjmp with siglongjmp.
 * the data type sig_atomic_t defined by the ISO C standard to be the type of variable that can be written without being interrupted.
 */
static volatile /*int*/sig_atomic_t *setjmpbuffers_state;
static volatile /*bool*/ sig_atomic_t *dummyinHz;
//extern pthread_t registeredThreads[MAX_TID_POW2];
//extern void *errnoThreads[MAX_TID_POW2];
//extern sigjmp_buf *setjmpbuffers;

static debugCounter countInterrupted(MAX_TID_POW2);
static debugCounter countLongjmp(MAX_TID_POW2);
//extern debugCounter countInterrupted;
//extern debugCounter countLongjmp;
#define MAX_THREAD_ADDR 10000

#ifdef CRASH_RECOVERY_USING_SETJMP
#define CHECKPOINT_AND_RUN_UPDATE(tid, finishedbool) \
    if (MasterRecordMgr::supportsCrashRecovery() && sigsetjmp(setjmpbuffers[(tid)], 0)) { \
        recordmgr->enterQuiescentState((tid)); \
        (finishedbool) = recoverAnyAttemptedSCX((tid), -1); \
        recordmgr->recoveryMgr->unblockCrashRecoverySignal(); \
    } else
#define CHECKPOINT_AND_RUN_QUERY(tid) \
    if (MasterRecordMgr::supportsCrashRecovery() && sigsetjmp(setjmpbuffers[(tid)], 0)) { \
        recordmgr->enterQuiescentState((tid)); \
        recordmgr->recoveryMgr->unblockCrashRecoverySignal(); \
    } else
#endif

template <class MasterRecordMgr>
void crashhandler(int signum, siginfo_t *info, void *uctx) {
    MasterRecordMgr * const recordmgr = (MasterRecordMgr * const) ___singleton;
#ifdef SIGHANDLER_IDENTIFY_USING_PTHREAD_GETSPECIFIC
    int tid = (int) ((long) pthread_getspecific(pthreadkey));
#endif
    /*TRACE*/ COUTATOMICTID("received signal "<<signum<<endl);

    // if i'm active (not in a quiescent state), i must throw an exception
    // and clean up after myself, instead of continuing my operation.
    DEBUG countInterrupted.inc(tid);
    __sync_synchronize();
    if (!recordmgr->isQuiescent(tid)) { //AJ: If not in hzwrite section restart using sigjmp
#ifdef PERFORM_RESTART_IN_SIGHANDLER
        recordmgr->enterQuiescentState(tid); //AJ id i was in non hz will endop and restart the op.
        DEBUG countLongjmp.inc(tid);
        __sync_synchronize();
#ifdef CRASH_RECOVERY_USING_SETJMP
        siglongjmp(setjmpbuffers[tid], 1);
#endif
#endif
    }
    // otherwise, i simply continue my operation as if nothing happened.
    // this lets me behave nicely when it would be dangerous for me to be
    // restarted (being in a Q state is analogous to having interrupts 
    // disabled in an operating system kernel; however, whereas disabling
    // interrupts blocks other processes' progress, being in a Q state
    // implies that you cannot block the progress of any other thread.)
}

//#define NUM_THR 6
//static volatile int ii[NUM_THR]={0,0}; //siglongjmp count
//static volatile int cc[NUM_THR]={0,0};//caught in handler count
//static volatile sig_atomic_t setjmp_res[NUM_THR] ={1,1};
////static volatile sig_atomic_t jmp_allowed[NUM_THR] ={1,1};
//
////atomic<int> setjmp_res[NUM_THR] ={1,1};
//static volatile atomic<int> jmp_allowed(1);

#if SIMPLE_RECOVERY_APPARATUS
template <class MasterRecordMgr>
extern void dummycrashhandler(int signum, siginfo_t *info, void *uctx);

//{
//    MasterRecordMgr * const recordmgr = (MasterRecordMgr * const) ___singleton;
//    int tid = (int) ((long) pthread_getspecific(pthreadkey));
//
////    static thread_local int k=0;
////    ++k;
//    cc[tid]++;
//    
//    if (setjmp_res [tid] != 0 || jmp_allowed != 1)
//        return;
//
//    if (setjmp_res [tid] == 0 && jmp_allowed.load() == 1){
//        ii[tid]++;
//         printf("tid=%d setjmp_res: %d jmp_allowed %d\n", tid, setjmp_res [tid], jmp_allowed.load() );
////        siglongjmp(setjmpbuffers[tid], 1);
//    }
////    printf("%d\n", tid);
//}



//template <class MasterRecordMgr>
//void dummycrashhandler(int signum, siginfo_t *info, void *uctx) {
//    MasterRecordMgr * const recordmgr = (MasterRecordMgr * const) ___singleton;
//    int tid = (int) ((long) pthread_getspecific(pthreadkey));
//    
//    if (dummyinHz[tid] == 1 || setjmpbuffers_state[tid] !=0)
//        return;
//    
////    COUTATOMICTID("---------------->received signal: "<<signum<<"will resume setjmpbuffers_state[tid]:"<<setjmpbuffers_state[tid]<<"dummyinHz[tid]:"<<dummyinHz[tid]<<endl);
//    /*
//     * FIXME: Ensure that siglongjmp doesnt occur for thread which is in mid of sys call. like a reclaimer might be freeing its retiredbag in leaveQstate().
//     * Longjumping and restarting such a thread may corrupt retired bag or delay freeing of bag. Here, a reclaimer thread is interrupted.
//     */
//    if (dummyinHz[tid] == false && setjmpbuffers_state[tid] == 0 /*&& (!recordmgr->isInHzSec(tid))&& not in Hz write sect*/) //if corresponding setjmp has occurred i.e env for this thread has been saved then only jmp.
//        siglongjmp(setjmpbuffers[tid], 1); //FIXME: check if setjmpbuffers[tid] is valid. If stack hasn't been saved then jump may happen at invalid location.
//
//}
#else
template <class MasterRecordMgr>
void dummycrashhandler(int signum, siginfo_t *info, void *uctx) 
{
    MasterRecordMgr * const recordmgr = (MasterRecordMgr * const) ___singleton;
    int tid = (int) ((long) pthread_getspecific(pthreadkey));
    
    if (setjmpbuffers_state[tid] !=0 || recordmgr->isInHzSec(tid) == 1)
        return;
    
    //COUTATOMICTID("---------------->received signal: "<<signum<<"will resume setjmpbuffers_state[tid]:"<<setjmpbuffers_state[tid]<<"dummyinHz[tid]:"<<dummyinHz[tid]<<endl);
    //if (setjmpbuffers_state[tid] == 0 && (!recordmgr->isInHzSec(tid)) ) //if corresponding setjmp has occurred i.e env for this thread has been saved then only jmp.
        siglongjmp(setjmpbuffers[tid], 1); //FIXME: check if setjmpbuffers[tid] is valid. If stack hasn't been saved then jump may happen at invalid location.

    /*
     * FIXME: Ensure that siglongjmp doesnt occur for thread which is in mid of sys call. like a reclaimer might be freeing its retiredbag in leaveQstate().
     * Longjumping and restarting such a thread may corrupt retired bag or delay freeing of bag. Here, a reclaimer thread is interrupted.
     */
}
#endif//#if SIMPLE_RECOVERY_APPARATUS



template <class MasterRecordMgr>
class RecoveryMgr {
public:
    const int NUM_PROCESSES;
    const int neutralizeSignal;
    
    inline int getTidInefficient(const pthread_t me) {
        int tid = -1;
        for (int i=0;i<NUM_PROCESSES;++i) {
            if (pthread_equal(registeredThreads[i], me)) {
                tid = i;
            }
        }
        // fail to find my tid -- should be impossible
        if (tid == -1) {
            COUTATOMIC("THIS SHOULD NEVER HAPPEN"<<endl);
            assert(false);
            exit(-1);
        }
        return tid;
    }
    inline int getTidInefficientErrno() {
        int tid = -1;
        for (int i=0;i<NUM_PROCESSES;++i) {
            // here, we use the fact that errno is defined to be a thread local variable
            if (&errno == errnoThreads[i]) {
                tid = i;
            }
        }
        // fail to find my tid -- should be impossible
        if (tid == -1) {
            COUTATOMIC("THIS SHOULD NEVER HAPPEN"<<endl);
            assert(false);
            exit(-1);
        }
        return tid;
    }
    inline int getTid_pthread_getspecific() 
    {
        void * result = pthread_getspecific(pthreadkey);
        if (!result) {
            assert(false);
            COUTATOMIC("ERROR: failed to get thread id using pthread_getspecific"<<endl);
            exit(-1);
        }
        return (int) ((long) result);
    }
    inline pthread_t getPthread(const int tid) 
    {
        return registeredThreads[tid];
    }
    
    void initThread(const int tid) 
    {
        // create mapping between tid and pthread_self for the signal handler
        // and for any thread that neutralizes another
        registeredThreads[tid] = pthread_self();
        TRACE COUTATOMICTID("registeredThreads[tid] ="<< registeredThreads[tid]<<" pthread_self()="<<pthread_self()<<endl);
        
        // here, we use the fact that errno is defined to be a thread local variable
        errnoThreads[tid] = &errno;
        if (pthread_setspecific(pthreadkey, (void*) (long) tid)) {
            COUTATOMIC("ERROR: failure of pthread_setspecific for tid="<<tid<<endl);
        }
        const long __readtid = (long) ((int *) pthread_getspecific(pthreadkey));
        VERBOSE DEBUG COUTATOMICTID("did pthread_setspecific, pthread_getspecific of "<<__readtid<<endl);
        assert(__readtid == tid);
        TRACE COUTATOMICTID("created pthread tid mapping"<<endl);
    }
    
    /*A thread calling this becomes ready to recieve signal again and execute handler thus wont ignore signal*/
    void unblockCrashRecoverySignal() 
    {
        __sync_synchronize();
        sigset_t oldset;
        sigemptyset(&oldset);
        sigaddset(&oldset, neutralizeSignal);
        
        uint tid = getTidInefficient(pthread_self());
        if (pthread_sigmask(SIG_UNBLOCK, &oldset, NULL)) { //AJ: For me in presence of multiple threads calling this causes crash in testsigapparatus.
            VERBOSE COUTATOMIC("ERROR UNBLOCKING SIGNAL"<<endl);
            exit(-1);
        }
        COUTATOMICTID("UNBLOCKED Signals "<<neutralizeSignal<<endl); 
    }
    
    RecoveryMgr(const int numProcesses, const int _neutralizeSignal, MasterRecordMgr * const masterRecordMgr)
            : neutralizeSignal(_neutralizeSignal), NUM_PROCESSES(numProcesses) {
        
        setjmpbuffers = new sigjmp_buf[numProcesses];
        setjmpbuffers_state = new sig_atomic_t[numProcesses];
        dummyinHz = new sig_atomic_t[numProcesses];
        for (int i = 0 ; i < NUM_PROCESSES; i++)
        {
            setjmpbuffers_state[i] = -1; //initial state to tell that setjmp hasn't yet occurred.
            dummyinHz[i] = false;
        }
        pthread_key_create(&pthreadkey, NULL);
        
        if (MasterRecordMgr::supportsCrashRecovery()) 
        {
            // set up crash recovery signal handling for this process
            memset(&___act, 0, sizeof(___act));
            //___act.sa_sigaction = crashhandler<MasterRecordMgr>; // specify signal handler
            ___act.sa_sigaction = dummycrashhandler<MasterRecordMgr>; // specify tpp signal handler
            ___act.sa_flags = SA_RESTART | SA_SIGINFO ; // restart any interrupted sys calls instead of silently failing
            sigfillset(&___act.sa_mask);               // block all possible signals during handler
            if (sigaction(_neutralizeSignal, &___act, NULL)) 
            {
                COUTATOMIC("ERROR: could not register signal handler for signal "<<_neutralizeSignal<<endl);
                assert(false);
                exit(-1);
            } else 
            {
                /*VERBOSE*/ COUTATOMIC("registered signal "<<_neutralizeSignal<<" for crash recovery. All signals are blocked in sig handler."<<endl);
            }
        }
        // set up shared pointer to this class instance for the signal handler
        ___singleton = (void *) masterRecordMgr;
    }
    ~RecoveryMgr() {
        delete[] setjmpbuffers;
    }
};

#endif	/* RECOVERY_MANAGER_H */

