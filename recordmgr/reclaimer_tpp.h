/* 
 * File:   reclaimer_tpp.h
 * Author: a529sing
 *
 * Created on November 30, 2019, 12:49 PM
 */

/*
 * GENERAL COMMENTS TO MYSELF:
 * Issue1: reclaimer threshold should make sure that an op doesn't retire records that exceed past the bag capacity.
 * Issue: another thread might change its hazardpointer list while reclaimer is preparing hash set to send record to pool.
 */

#ifndef RECLAIMER_TPP_H
#define RECLAIMER_TPP_H

#include <cassert>
#include <iostream>
#include "machineconstants.h" //not known
#include "globals.h"
#include "blockbag.h"
#include "allocator_interface.h"
#include "reclaimer_interface.h"
#include "arraylist.h"
#include "hashtable.h"
#include "record_manager_single_type.h" //not 

using namespace std;

#define MAX_PER_THREAD_HAZARDPTR 2
#define OPS_BEFORE_NEUTRALIZE 1000

template <typename T = void, class Pool = pool_interface<T> >
class reclaimer_tpp : public reclaimer_interface<T, Pool>{
    private:
        blockbag<T> **retiredblockbag;  //retiredbag[tid] = retired nodes collected by tid so far in its execution.
        /*Earlier this was not atomic which was buggy as Hzptrs if not published right away could cause crash as signal delivery is asyn.
         Following case may occur: reclaimer send sig--> collects all Hzptrs (say misses reading hzptrs of tidx)--> frees all records. Now tidx hasnt received sig
         moves to inHzsect and now nothing prevents tidx to access freed record.
         */
        AtomicArrayList<T> **acquired_hazardptrs; // acquired_hazardptrs[tid] =  announced hazard pointers by tid before entering Hazardous write section.
        hashset_new<T> **acquired_hazardptr_set;    //acquired_hazardptr_set[tid] = a list of all hazard pointer announced by other threads seen by tid.

        //signal vars
        sigset_t neutralizeSignalSet;
        //atomic<bool> *in_hazardsect;
        sig_atomic_t *in_hazardsect;    // in_hazardsect[tid] == 1 : donot siglongjmp from handler, just resume execution. Else siglongjmp if a valid sigsetjmp exist
        volatile long *min_ops_before_neutralize;
        volatile bool neutralize_token; //testing: so that only one thread gets exclusive right to signalling.

        /*Send neutralize signal to all other threads except self*/
        bool neutralizeOthers(const int tid)
        {
            for(int otherTid = 0; otherTid < this->NUM_PROCESSES; ++otherTid)
            {
                if (tid != otherTid) 
                {
                    pthread_t otherPthread = this->recoveryMgr->getPthread(otherTid);
                    int error = 0;
                    if (error = pthread_kill(otherPthread, this->recoveryMgr->neutralizeSignal) )//SIGQUIT
                    {
                        COUTATOMICTID("?ERROR?"<<error<<" when trying to pthread_kill(pthread_tFor("<<otherTid<<"), "<<this->recoveryMgr->neutralizeSignal<<")"<<std::endl);
                        return false;
                    }
                    else
                    {
                        COUTATOMICTID(" Signal sent via pthread_kill(pthread_tFor("<<otherTid<<"), "<<this->recoveryMgr->neutralizeSignal<<")"<<std::endl);
                    }
                }//if (tid != otherTid) 
            }//for(int otherTid = 0
            return true;
        }

        inline bool attemptNeutralization(const int tid)
        {
            //TRACE COUTATOMICTID("tpp: attemptNeutralization"<<endl);
#if 1
            //temp only tid 0 can neutralize
            if (tid != 0 )
            {
                return false;
            }
            COUTATOMICTID("We have:("<<retiredblockbag[tid]->getSizeInBlocks()<<"blocks)"<<retiredblockbag[tid]->computeSize()<<" things waiting to be retired..."<<endl);
            bool res = neutralizeOthers(tid);
            if(false  == res)
            {
                COUTATOMICTID("ERROR while sending pthread_kill"<<endl);
                exit (-1);
            }
            else
            {
                return true;  
            }
#else
            return true;// temp to check that free works with no signals 
#endif
        }
        
    public:
        
        /*NOTICEME: what if another thread overwrites its announced Hzptrs I am about to read or in mid of reading? 
         * Need to be careful in design. Ans: I think its fine if we read the latest value then we are protecting the HP. 
         * If HP is stale then our retired bag cannt contain the latest HP.
         *Note not placing reclaimer code in start op may require two fences for in_hazard section
         */

        //variadic template concept
        inline bool leaveQuiescentState(const int tid, void * const * const reclaimers, const int numReclaimers)
        {
            assert("this thread should have no Hzptrs as it hasnt started acquiring hzptrs" && acquired_hazardptrs[tid]->isEmpty());
                
            if( retiredblockbag[tid]->computeSize()> 1000 && attemptNeutralization(tid)) //think of performance friendly threshold.
            {
                //At this point all threads 
                //1. Either have RESTARTED: implies all threads forgot pointers tid wanna reclaim.
                //2. Or are inHazardous write section and thus may have pointers to record tid wanna reclaim.
                //Skip sending all records to pool for case 2. Such records are HP protected.
                
                blockbag<T> * const freeable = retiredblockbag[tid];
                acquired_hazardptr_set[tid]->clear();
                assert(acquired_hazardptr_set[tid]->size() == 0);
                /*
                 * Other threads may have 1. full Hzptr list 2. partially filled Hzptr list 3. completely empty (all null ptrs) Hzptr list.
                 */
                for (int otherTid = 0; otherTid <  this->NUM_PROCESSES; ++otherTid)
                {
                    int sz = acquired_hazardptrs[otherTid]->size(); //size shouldn't change during execution.
                    assert (sz <= MAX_PER_THREAD_HAZARDPTR);
                    for (int ixHP = 0; ixHP < sz/*Can do this as sz is const*/; ++ixHP)
                    {
                        T * hp = (T*) acquired_hazardptrs[otherTid]->get(ixHP);//acquired_hazardptrs size never changes.
                        if (hp)
                        {
                            acquired_hazardptr_set[tid]->insert((T*)hp);
                        }
                    }                    
                }
                
                COUTATOMICTID("Before freeing freeable-bag size in nodes:"<<freeable->computeSize()<<endl);
//                COUTATOMICTID("Hazardptr set size which tid decided won't be freed:"<< acquired_hazardptr_set[tid]->size()<<endl);
                assert (acquired_hazardptr_set[tid]->size() < freeable->computeSize());//psudo correct check to verify all objects to be retired are not hz protected.

                //Now collected all hazard pointer protected records. Send my retired records to pool except the Hp protected ones.
                blockbag_iterator<T> it =  freeable->begin();
                blockbag_iterator<T> nextswap =  freeable->begin();
                while(it != freeable->end())
                {
                    if (acquired_hazardptr_set[tid]->contains(*it))
                    {
                        it.swap(nextswap.getCurr(), nextswap.getIndex());
                        nextswap++;
                    }
                    it++;
                }
                block<T>* const curr = nextswap.getCurr();
                if (curr){
                    this->pool->addMoveFullBlocks(tid, freeable, curr);
                }
            
                COUTATOMICTID("After freeing retiredblockbag size in nodes:"<<retiredblockbag[tid]->computeSize()<<endl<<endl);
//                COUTATOMICTID("acquired_hazardptr_set[tid]->size():"<<acquired_hazardptr_set[tid]->size()<<endl);
                
                if (acquired_hazardptr_set[tid]->size() == 0) 
                    assert(retiredblockbag[tid]->computeSize() == 0 );
                
                //COUTATOMICTID("after neutralize all and free, sz in blocks:"<< retiredblockbag[tid]->getSizeInBlocks()<<", sz in objects:"<<retiredblockbag[tid]->computeSize()<<endl);
                //COUTATOMICTID("set token to False"<<endl);
                //__sync_bool_compare_and_swap(&neutralize_token, true, false);
            }//if( retiredbag[tid]->isFull() )
        }        
        
        
        //template aliasing concept
        template<typename _Tp1>
        struct rebind {
            typedef reclaimer_tpp<_Tp1, Pool> other;
        };
        template<typename _Tp1, typename _Tp2>
        struct rebind2
        {
            typedef reclaimer_tpp<_Tp1, _Tp2> other;
        };
        
        inline static bool quiescenceIsPerRecordType() { 
            TRACE COUTATOMIC("tpp: quiescenceIsPerRecordType"<<endl); 
            return false; 
        }//copying depraplus
        
        inline static bool supportsCrashRecovery() {
            TRACE COUTATOMIC("tpp: supportsCrashRecovery"<<endl); 
            return true; 
        }
        
        inline bool isInHzSec(const int tid)
        {
            TRACE COUTATOMICTID("tpp: isInHzSec"<<endl);
            return in_hazardsect[tid];
        }
        
        /*Not using, exists only to adhere to existing code*/
        inline bool isQuiescent(const int tid)
        {
//            COUTATOMICTID("tpp: isQuiescent"<<endl);
//            return ( in_hazardsect[tid]); //if I am in hazardous write section means I am Quiescent and need to continue my Op. Else need to restart. 
                                                    //Semantic to reuse Debraplus signal handler
            return true;                      //temporarily to ensure no assert in legacy code (not using in tpp code)                    
        }
        
        inline /*static*/ bool isProtected(const int tid, T * const obj) {
//            TRACE COUTATOMICTID("tpp: isProtected"<<endl);
            return in_hazardsect[tid];
//            return true;
        }

        /*Hijack: Call this to tell all threads that I am in Hzwrite section.*/
        inline /*static */bool protect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) {
            TRACE COUTATOMICTID("tpp: protect"<<endl);
            in_hazardsect[tid] = true; //store
            return true; //not checking ret val in tpp
        }
        
        /*Hijack: Unused enterQuiescentState tells that I am in not in Hzwrite sect anymore */
        inline /*static*/ void unprotect(const int tid, T * const obj) 
        {
            TRACE COUTATOMICTID("tpp: unprotect"<<endl);
            in_hazardsect[tid] = false; //store. Can get rid of implicit fence.
        }
        
        /*Hijack: to save the Hazard ptrs*/
        inline bool isQProtected(const int tid, T * const obj) {
            TRACE COUTATOMICTID("tpp: isQProtected"<<endl);
            return acquired_hazardptrs[tid]->contains(obj); // this is inefficient, but should only happen when recovering from being neutralized...
        }
        
        /*Hijack: to remove the saved Hazard ptrs*/
        inline bool qProtect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true){
            TRACE COUTATOMICTID("tpp: qProtect"<<endl);
            assert(acquired_hazardptrs[tid]);
            acquired_hazardptrs[tid]->add(obj); //protect the data to be used in Hzwrite section 
            
            //__sync_synchronize();
            //Not needed. Because 1. If  inHzsect == true --> Reclaimer guarantees not to free tid Hzptrs. 
            //2. If reclaimer decides to free these Hzptrs then inHzsect = false which means tid will restart and wont use its Hzptrs.. 
               
        }
        
        /*CHECKME: donot erase  acquired HP. Leave as it is. reason: setting size 0 may crash a thread who is afterneutralizing all is reading acquired_hazardptrs*/
        inline void qUnprotectAll(const int tid) 
        {
            TRACE COUTATOMICTID("tpp: qUnprotectAll"<<endl);
//            TRACE COUTATOMICTID("reclaimer_tpp::unprotectAllObjectsEvenIfQuiescent(tid="<<tid<<")"<<endl);
            //assert(isQuiescent(tid));
            acquired_hazardptrs[tid]->clear();//Vulnerable pt. What if reclaimer is reading this hzp list to free.? Hope is, at this point reclaimer can free as the op is done with the 
                                                                    //use of hzp nodes. even if reclaimers read partial list no harm done.
//            assert(acquired_hazardptrs[tid]->size() == 0);
//            acquired_hazardptrs[tid]->erase(obj); 
        }
        
        /*end HazardousWrite. */
        inline void enterQuiescentState(const int tid)
        {
            TRACE COUTATOMICTID("tpp: enterQuiescentState"<<endl);
            //cannot clear acquired_hazardptrs
            //acquired_hazardptrs[tid]->clear();//from crash handler if need to restart to alternate path
            /*
             * If called from alternate path. in_hazardsect already false. No harm done in repeating this. Umm probably implicit fence!!: FIXME.
             * Get rid of any Hz pointers before going Quiescent.
             * Make sure this is always called when the op is about to leave and their is a guarantee that nexttime a ds node is access only via root/head.
             */
            qUnprotectAll(tid); //clear own Hzptrs
            setjmpbuffers_state[tid] = -1; //set thread in non sigjmp-able state
            unprotect(tid, NULL);   //ens Hzrwd write sect
        }

        inline void retire (const int tid, T* p)
        {
            TRACE COUTATOMICTID("tpp: retire"<<endl);
            retiredblockbag[tid]->add(p);
        }

        void debugPrintStatus (const int tid)
        {
            cout << "debug print status reclaimer tpp"<<endl;
        }

        //constructor and destructor
        reclaimer_tpp (const int numProcess, Pool *_pool, debugInfo * const _debug, RecoveryMgr<void *> * const _recoveryMgr = NULL) 
                : reclaimer_interface<T, Pool> (numProcess, _pool, _debug, _recoveryMgr)
        {
            /*TRACE COUTATOMIC("constructor reclaimer_tpp"<<endl);
            //init signal apparatus
            if(_recoveryMgr) cout<<"SIGRTMIN="<<SIGRTMIN<<" neutralizeSignal="<<this->recoveryMgr->neutralizeSignal<<std::endl;

            if(sigemptyset(&neutralizeSignalSet))
            {
                TRACE COUTATOMIC("error creating empty signal set"<<endl);
                exit(-1);
            }

            if(_recoveryMgr)
            {
                if( sigaddset (&neutralizeSignalSet, this->recoveryMgr->neutralizeSignal) )
                {
                    TRACE COUTATOMIC("error adding signal to signal set"<<endl);
                    exit (-1);
                }
            }

            //init ds /*TODO: Memory layout */
            neutralize_token = false;
            min_ops_before_neutralize = new long [numProcess];
            acquired_hazardptrs = new AtomicArrayList<T> * [numProcess];
            acquired_hazardptr_set = new hashset_new<T> * [numProcess];
            retiredblockbag = new blockbag<T>*[numProcess];

            //in_hazardsect = new atomic<bool> [numProcess];
            in_hazardsect = new sig_atomic_t [numProcess]; //reliable type provided by posix signal
            
            for (int tid = 0; tid < numProcess; ++tid)
            {
                acquired_hazardptrs[tid] = new AtomicArrayList<T>(MAX_PER_THREAD_HAZARDPTR); //never clear or erase this ds just overwrite ne HPs
                acquired_hazardptr_set[tid] = new hashset_new<T>(numProcess*MAX_PER_THREAD_HAZARDPTR); //worst case all could be in hzwrite section thus a tid may have this many hp.
                retiredblockbag[tid] = new blockbag<T>(this->pool->blockpools[tid]);
                in_hazardsect[tid] = 0;
                
                //REMOVEMELATER:
                min_ops_before_neutralize[tid] = OPS_BEFORE_NEUTRALIZE;
                
                TRACE COUTATOMICTID("reclaimer_tpp::retiredblockbag size:"<<retiredblockbag[tid]->getSizeInBlocks()<<" "<<pthread_self()<<endl);
            }
            //std::memset(in_hazardsect, 0, numProcess);
        }

        ~reclaimer_tpp()
        {
            COUTATOMIC("destructor reclaimer_tpp"<<endl);
            for (int tid = 0; tid < this->NUM_PROCESSES; ++tid)
            {
                int sz = retiredblockbag[tid]->computeSize();
                COUTATOMICTID("retired bag size:"<<sz<<endl);
                this->pool->addMoveAll(tid, retiredblockbag[tid]);
                delete retiredblockbag[tid];
                
                delete acquired_hazardptrs[tid];
                delete acquired_hazardptr_set[tid];
            }
            delete[] acquired_hazardptrs;
            delete[] acquired_hazardptr_set;
            delete[] in_hazardsect;
            delete[] min_ops_before_neutralize;
        }         
};

#endif /* RECLAIMER_TPP_H */

